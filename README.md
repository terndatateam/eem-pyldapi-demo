> demo using pyLDAPI for EEM data

## Getting started for developers

```bash
mkdir venv
virtualenv -p python3 venv/
. venv/bin/activate
pip install -r requirements.txt
python main.py
# open browser to http://localhost:8081/
```

This API supports content negotiation so you can get `text/html` or `application/json`.
