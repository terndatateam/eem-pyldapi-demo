import json

import requests
from flask import Flask, Response, request, render_template
from pyldapi import setup as pyldapi_setup
from pyldapi import RegisterOfRegistersRenderer, RegisterRenderer, Renderer, View

API_BASE = 'http://127.0.0.1:8081'
backend_api_url_base = 'http://db-test.tern.org.au/eem'

MyPetView = View("PetView", "A profile of my pet.", ['text/html', 'application/json'],
                 'application/json', namespace="http://example.org/def/mypetview")

app = Flask(__name__)


class TernRenderer(Renderer):
    def __init__(self, request, uri, instance, pet_html_template, **kwargs):
        self.views = {'mypetview': MyPetView}
        self.default_view_token = 'mypetview'
        super(TernRenderer, self).__init__(
            request, uri, self.views, self.default_view_token, **kwargs)
        self.instance = instance
        self.pet_html_template = pet_html_template

    def _render_mypetview(self):
        self.headers['Profile'] = 'http://example.org/def/mypetview'
        if self.format == "application/json":
            return Response(json.dumps(self.instance),
                            mimetype="application/json", status=200)
        elif self.format == "text/html":
            return Response(render_template(self.pet_html_template, **self.instance))

    def render(self):
        response = super(TernRenderer, self).render()
        if not response and self.view == 'mypetview':
            response = self._render_mypetview()
        else:
            raise NotImplementedError(self.view)
        return response


@app.route('/id/site/<string:site_id>')
def site_instance(site_id):
    instance = None
    resp = requests.get(backend_api_url_base + '/eemsitedata?siteId=eq.' + site_id)
    instance = resp.json()[0]
    if instance is None:
        return Response("Not Found", status=404)
    renderer = TernRenderer(request, request.base_url, instance, 'site.html')
    return renderer.render()


@app.route('/sites')
def sites_reg():
    resp = requests.get(backend_api_url_base + '/eemsitedata?limit=10&select=siteId')
    # assert r.status_code == 200
    sites = resp.json()
    site_items = [("/id/site/{}".format(i['siteId']), i['siteId']) for i in sites]
    r = RegisterRenderer(request,
                         API_BASE + '/sites',
                         "Sites Register",
                         "A complete register of sites.",
                         site_items,
                         ["http://example.com/Site"], # FIXME correct this URI
                         len(site_items),
                         super_register=API_BASE + '/'
                         )
    return r.render()


@app.route('/id/observation/<string:obs_id>')
def obs_instance(obs_id):
    instance = None
    resp = requests.get(backend_api_url_base + '/eemobservations?id=eq.' + obs_id)
    instance = resp.json()[0]
    if instance is None:
        return Response("Not Found", status=404)
    renderer = TernRenderer(request, request.base_url, instance, 'observation.html')
    return renderer.render()


@app.route('/observations')
def obs_reg():
    resp = requests.get(backend_api_url_base + '/eemobservations?limit=10&select=id')
    # assert r.status_code == 200
    observations = resp.json()
    obs_items = [("/id/observation/{}".format(i['id']), i['id']) for i in observations] # FIXME build some meaningful name
    r = RegisterRenderer(request,
                         API_BASE + '/observations',
                         "Observation Register",
                         "A complete register of observations.",
                         obs_items,
                         ["http://example.com/Observation"], # FIXME correct this URI
                         len(obs_items),
                         super_register=API_BASE + '/'
                         )
    return r.render()


@app.route('/')
def index():
    rofr = RegisterOfRegistersRenderer(request,
                                       API_BASE,
                                       "Register of Registers",
                                       "A register of all of my registers.",
                                       "./rofr.ttl"
                                       )
    return rofr.render()


if __name__ == "__main__":
    pyldapi_setup(app, '.', API_BASE)
    app.run("127.0.0.1", 8081, debug=True, threaded=True, use_reloader=False)